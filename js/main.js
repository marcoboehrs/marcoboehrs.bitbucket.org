(function() {
    loadOptions();
    submitHandler();
})();

function submitHandler() {
    var $submitButton = $('#submitButton');

    $submitButton.on('click', function(){
        var return_to = getQueryParam("return_to", "pebblejs://close#");
        document.location = return_to + encodeURIComponent(JSON.stringify(getAndStoreConfigData()));
    });
}

function loadOptions(){
    var serverUrl = document.getElementById("serverUrl");
    if(localStorage["serverUrl"]) {
        serverUrl.value = localStorage.getItem("serverUrl");              
    }
}

function getQueryParam(variable, defaultValue) {
    var query = location.search.substring(1);
    var vars = query.split('&');
    for (var i = 0; i < vars.length; i++) {
        var pair = vars[i].split('=');
        if(pair[0] === variable) {
            return decodeURIComponent(pair[1]);
        }
    }
    return defaultValue || false;
}

function getAndStoreConfigData(){
    var serverUrl = document.getElementById("serverUrl");
           
    var options = {
        serverUrl: serverUrl.value
    };
    
    localStorage.setItem('serverUrl', options['serverUrl']);    
    return options;
}